<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Services\ArticleSaveInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    /**
     * @Route("/")
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function showAllArticles(ArticleRepository $articleRepository){
        $articles = $articleRepository->getNewestToOldest();
        return $this->render('homepage.html.twig', [
            'articles' => $articles
        ]);
    }


    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/article/new")
     * @return Response
     */
    public function createNewArticle(){
        return $this->render('article/newArticle.html.twig', [
            'article' => new Article()
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/article/edit/{id}")
     * @param Article $article
     * @return Response
     */
    public function editExistingArticle(Article $article){
        return $this->render('article/newArticle.html.twig',[
            'article' => $article
        ]);
    }

    /**
     *
     * @IsGranted("ROLE_ADMIN")
     * @Route("/article/save", methods="POST")
     * @param ArticleSaveInterface $articleSave
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveArticle(ArticleSaveInterface $articleSave, Request $request){
        $articleSave->saveArticle($request->request->get('title'), $request->request->get('article'), $request->request->get('id'));
        return $this->redirectToRoute('app_article_showallartilces');
    }

    /**
     * @Route("/article/show/{id}")
     * @param Article $article
     * @return Response
     */
    public function showArticle(Article $article){
        return $this->render('article/article.html.twig',[
            'article' => $article
        ]);
    }
}
