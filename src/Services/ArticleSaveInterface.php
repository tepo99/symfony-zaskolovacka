<?php


namespace App\Services;


interface ArticleSaveInterface
{
    public function saveArticle($title, $body, $id=null):void;
}