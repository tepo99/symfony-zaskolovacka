<?php


namespace App\Services;


use App\Entity\Article;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ArticleSaveService implements ArticleSaveInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function saveArticle($title, $body, $id = null): void
    {
        if($id) {
            $article = $this->entityManager->getRepository(Article::class)->find($id);
        } else {
            $article = new Article();
        }
        $article->setTitle($title);
        $article->setArticle($body);
        $article->setInsertDate(new DateTime('now'));
        $this->entityManager->persist($article);
        $this->entityManager->flush();
    }
}